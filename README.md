This application is an apply test for the fullstack position offered by SocialPoint, developed by [Pau Aza Puig](https://www.linkedin.com/in/pauazap/)

---

#### Application run instructions
##### To run the application with the NodeJS server
Access the terminal and run the following command to prepare the environment and install Express server:
> `npm install`

To start the server, run the following command:
> `node server.js`

Open any web browser and access the following URL, to access the application:
> [http://localhost:8080](http://localhost:8080)

##### To run the application without the NodeJS server
Just open the `index.html` file (located into the root application folder) in any web browser.

#### Code repository
You can access the following URL to download the code from a private repository and view all the git commits and the work process:
> [https://bitbucket.org/pauazap/socialpointtest](https://bitbucket.org/pauazap/socialpointtest)

You can also download the repository, accessing to a terminal and typing the following command:
> `git clone https://bitbucket.org/pauazap/socialpointtest.git`

Then you just have to follow the steps described above to run the application.
