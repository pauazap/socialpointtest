const express = require('express');
const path = require('path');

const app = express();

app.use('/css', express.static('./css/'));
app.use('/js', express.static('./js/'));

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname + '/index.html'));
});

app.listen(8080);
