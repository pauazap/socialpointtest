// Button properties
var colorButtons = document.querySelectorAll('.circle');
var playButton = document.getElementById('playBtn');

// Button sequence properties
var buttonSequence = [];
var currentIndex = 0;
var sequenceCount = 3;
var userCanInteract = false;

// Sequence total wait time
var buttonClassTime = 500;
var nextButtonTime = 200;
var userWaitTime = 0;

// Disable color buttons interaction
toggleDisableColorButtons();

// Set click button on each of the color buttons
for (var i = 0; i < colorButtons.length; i++) {
  colorButtons[i].addEventListener('click', function(event) {
    if (userCanInteract) {
      clickButton(this.getAttribute('data-id'));
    }
  });
}

// Listen for Play button click
playButton.addEventListener('click', function() {
  startGame();
});

/**
 * Starts the game, generating a random color sequence
 */
function startGame() {
  buttonSequence = [];
  for (var i = 0; i <= sequenceCount; i++) {
    addButtonToSequence();
  }

  // Disable play button
  playButton.setAttribute('disabled', 'disabled');

  // Start game
  startSequence();
}

/**
 * Shows the color sequence to the user
 */
function startSequence() {
  userCanInteract = false;
  showCurrentButton();
  userWaitTime = (buttonClassTime + nextButtonTime) * buttonSequence.length;
  setTimeout(function() {
    userCanInteract = true;
    toggleDisableColorButtons();
  }, userWaitTime);
}

/**
 * Show to the user the current button into the color sequence
 * 
 */
function showCurrentButton(index = 0) {
  colorButtons[buttonSequence[index]].classList.add('active');
  setTimeout(function() {
    colorButtons[buttonSequence[index]].classList.remove('active');
    setTimeout(function() {
      if (index + 1 < buttonSequence.length) {
        showCurrentButton(index + 1);
      }
    }, nextButtonTime);
  }, buttonClassTime);
}

/**
 * When a user clicks a color button, check if is the correct button in sequence
 * 
 * - If the button is correct, add a button to the sequence and replay it
 * - If the button is inccorect, show an alert and restart the game
 */
function clickButton(buttonId) {
  if (buttonSequence[currentIndex] == buttonId) {
    currentIndex++;

    // If user entered the right combination, reset the counter and add an element
    if (currentIndex == buttonSequence.length) {
      currentIndex = 0;
      addButtonToSequence();
      toggleDisableColorButtons();
      startSequence();
    }
  } else {
    // Reset game and show error alert
    toggleDisableColorButtons();
    buttonSequence = [];
    currentIndex = 0;
    userCanInteract = false;
    playButton.removeAttribute('disabled');
    alert('You missed! Try again? :)');
  }
}

/**
 * Adds a button to the color button sequence
 */
function addButtonToSequence() {
  buttonSequence.push(Math.floor(Math.random() * colorButtons.length));
}

/**
 * Toggles the color buttons to be or not to be interactable
 */
function toggleDisableColorButtons() {
  for (var i = 0; i < colorButtons.length; i++) {
    if (colorButtons[i].className.includes('blocked')) {
      colorButtons[i].classList.remove('blocked');
    } else {
      colorButtons[i].classList.add('blocked');
    }
  }
}
